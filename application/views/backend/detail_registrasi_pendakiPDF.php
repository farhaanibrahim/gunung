<?php foreach($pendaki->result() as $row){} ?>
<style>
	.cell {
		border-collapse: collapse;
		border: 1px solid black;
	}
	
	.anggota{
		width: 100%;
		border-collapse: collapse;
		border: 1px solid black;
	}
	.sewa{
		margin-left: 10px;
		width: 100%;
	}
	table {
		width: 100%;
	}
	
	.space{
		width: 50%;
		float: left;
	}
	.cop{
		text-align: center;
	}
	.ttd_ketua{
		position: absolute;
		bottom: 10px;
		text-align: right;
	}
</style>

<div class="cop">
	<h1>LEMBAR PENDAFTARAN PENDAKIAN</h1>
</div>

<h3>Informasi Pendaftaran</h3>
<table>
	<tr>
		<td>Tujuan Pendakian</td>
		<td>:</td>
		<td class="cell"><?php echo $row->nm_gunung; ?></td>
	</tr>
	<tr>
		<td>Tanggal Pendakian</td>
		<td>:</td>
		<td class="cell"><?php echo $row->tgl_pendakian; ?></td>
	</tr>
	<tr>
		<td>Akhir Pendakian</td>
		<td>:</td>
		<td class="cell"><?php echo $row->akhir_pendakian; ?></td>
	</tr>
</table>

<h3>Ketua Kelompok</h3>
<table>
	<tr>
		<td>Ketua</td>
		<td>:</td>
		<td class="cell"><?php echo $row->nama_lengkap; ?></td>
	</tr>
	<tr>
		<td>NIK</td>
		<td>:</td>
		<td class="cell"><?php echo $row->no_ktp; ?></td>
	</tr>
	<tr>
		<td>Jenis Kelamin</td>
		<td>:</td>
		<td class="cell"><?php echo $row->jenis_kelamin; ?></td>
	</tr>
	<tr>
		<td>Tanggal Lahir</td>
		<td>:</td>
		<td class="cell"><?php echo $row->tgl_lahir; ?></td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>:</td>
		<td class="cell"><?php echo $row->alamat_rumah; ?></td>
	</tr>
	<tr>
		<td>Handphone</td>
		<td>:</td>
		<td class="cell"><?php echo $row->no_hp; ?></td>
	</tr>
	<tr>
		<td>Telp. Rumah</td>
		<td>:</td>
		<td class="cell"><?php echo $row->telfon_rumah; ?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td>:</td>
		<td class="cell"><?php echo $row->email; ?></td>
	</tr>
</table>

<div class="space">
	<h3>Anggota Pendaki</h3>
									<table class="anggota">
										<tr class="anggota">
											<td class="anggota">No.</td>
											<td class="anggota">Nama</td>
											<td class="anggota">NIK</td>
										</tr>
										<?php $no = 1; ?>
										<?php foreach($anggota->result() as $a): ?>
										<tr class="anggota">
											<td class="anggota"><?php echo $no; ?></td>
											<td class="anggota"><?php echo $a->nama_lengkap; ?></td>
											<td class="anggota"><?php echo $a->no_ktp; ?></td>
										</tr>
										<?php $no++; ?>
										<?php endforeach; ?>
									</table>
</div>
<div class="space">
<h3 class="sewa">Informasi Sewa Barang</h3>
<table class="sewa">
	<tr>
		<td>Sleeping Bag</td>
		<td>:</td>
		<td class="cell"><?php echo $row->sb." Buah"; ?></td>
	</tr>
	<tr>
		<td>Tenda</td>
		<td>:</td>
		<td class="cell"><?php echo $row->tenda." Buah"; ?></td>
	</tr>
	<tr>
		<td>Alat Masak</td>
		<td>:</td>
		<td class="cell"><?php echo $row->alat_masak." Buah"; ?></td>
	</tr>
</table>
</div>

<div class="ttd_ketua">
	<p>Ketua Kelompok</p>
	<br>
	<br>
	<p>(<?php echo $row->nama_lengkap; ?>)</p>
</div>

						
									