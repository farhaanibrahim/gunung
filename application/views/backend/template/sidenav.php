<ul class="nav" id="main-menu">

    <li>
        <a class="active-menu" href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
    </li>
    <li>
        <a href="<?php echo base_url('admin/news'); ?>"><i class="fa fa-fw fa-file"></i> News</a>
    </li>
    <li>
        <a href="<?php echo base_url('admin/gunung'); ?>"><i class="fa fa-fw fa-file"></i> Quota Gunung</a>
    </li> 
	<li>
		<a href="<?php echo base_url('admin/registrasi_pendaki'); ?>"><i class="fa fa-fw fa-user"></i> Lihat Pendaftar</a>
	</li>
</ul>