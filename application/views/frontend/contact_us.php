<!DOCTYPE html>
<html>

<head>
  <title>Contact Us</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/theme.css" type="text/css"> </head>

<body>
  <nav class="navbar navbar-expand-md bg-secondary navbar-dark">
    <div class="container">
    <a class="navbar-brand" href="<?php echo base_url(); ?>">Brand</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('news'); ?>">News</a>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('penyewaan'); ?>">Penyewaan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="<?php echo base_url('contact_us'); ?>">Contact us</a>
          </li>
        </ul>
        <form class="form-inline m-0"></form>
      </div>
    </div>
  </nav>
  <div class="py-5 text-center" style="background-image: url(<?php echo base_url('assets/frontend'); ?>/img/banner.png);">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12">
          <h1 class="display-3 mb-4 text-primary">Contact Us</h1><br><br><br><br>
        </div>
      </div>
    </div>
  </div>
  <div class="bg-dark text-white">
    <div class="container">
      <div class="row">
        <div class="p-4 col-md-3">
          <h2 class="mb-4 text-secondary">Keterangan</h2>
          <p class="text-white">Hubungi kontak disamping jika ada perasalahan dalam pendaftaran.</p><br><br><br><br>
        </div>
        <div class="p-4 col-md-3">
          <h2 class="mb-4">Contact</h2>
          <p>
            <a href="tel:+6281318965536" class="text-white">
              <i class="fa d-inline mr-3 text-secondary fa-phone"></i>0813-1896-5536</a>
          </p>
          <p>
            <a href="mailto:akbar.wibawanto@gmail.com" class="text-white">
              <i class="fa d-inline mr-3 text-secondary fa-envelope-o"></i>akbar.wibawanto@gmail.com</a>
          </p>
        </div>
                <div class="p-4 col-md-3">
          <h2 class="mb-4 text-secondary">Admin</h2>
          <a href="<?php echo base_url('admin'); ?>/" class="btn btn-primary">Login</a><br><br><br><br>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>