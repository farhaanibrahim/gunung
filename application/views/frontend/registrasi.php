<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/theme.css" type="text/css"> </head>
  <style type="text/css">
    
    .spacer{
      padding: 50px;
    }
  </style>
<body>
  <nav class="navbar navbar-expand-md bg-secondary navbar-dark">
    <div class="container">
    <a class="navbar-brand" href="<?php echo base_url(); ?>">Brand</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('news'); ?>">News</a>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('penyewaan'); ?>">Penyewaan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="<?php echo base_url('contact_us'); ?>">Contact us</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="py-5 text-center" style="background-image: url(<?php echo base_url('assets/frontend') ?>/img/banner.png);">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12">
          <h1 class="display-3 mb-4 text-primary">Form Pendaftaran</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
          <h1>Pendaftaran</h1>
          <p class="lead">Isilah data diri anda dengan benar</p><br><br>  
        </div>
      <div class="col-md-12">
		<?php if($this->session->flashdata('success')): ?>
			<div class="alert alert-success" role="alert">
			  <?php echo $this->session->flashdata('success'); ?>
			</div>
		<?php endif; ?>
        <form action="<?php echo base_url('registrasi/proses'); ?>" method="post" enctype="multipart/form-data">
          <h4><b>Pilih tanggal dan tujuan pendakian</b></h4>  
          <div class="form-group">
            <label><b>Tujuan Pendakian : </b></label>
            <select class="form-control" name="tujuan">
				<option>Pilih Tujuan Pendakian</option>
              <?php foreach($gunung->result() as $row): ?>
                <option value="<?php echo $row->id_gunung; ?>"><?php echo $row->nm_gunung; ?></option>
              <?php endforeach; ?>
             </select>
          </div>
          <div class="form-group">
            <label><b>Tanggal Pendakian</b></label>
            <input type="date" name="tgl_pendakian" class="form-control"><br>
			<input type="date" name="akhir_pendakian" class="form-control">
          </div>
          <h4><b>Data Diri</b></h4>
          <div class="form-group">
            <label><b>Nama : </b></label>
            <input type="text" name="nama_ketua" class="form-control">
          </div>
          <div class="form-group">
            <label><b>NIK : </b></label>
            <input type="text" name="nik_ketua" class="form-control">
          </div>
          <div class="form-group">
            <label><b>Jenis Kelamin : </b></label>
            <select class="form-control" name="jk">
              <option value="Laki-laki">Laki-laki </option>
              <option value="Perempuan">Perempuan </option>
             </select>
          </div>
		  <div class="form-group">
            <label><b>Tanggal Lahir</b></label>
            <input type="date" name="tgl_lahir" class="form-control"><br>
          </div>
          <div class="form-group">
            <label><b>Alamat : </b></label>
            <textarea class="form-control" name="alamat"></textarea>
          </div>  
		  <div class="form-group">
            <label><b>Handphone : </b></label>
            <input type="text" name="no_hp" class="form-control">
          </div>
		  <div class="form-group">
            <label><b>Telp. Rumah : </b></label>
            <input type="text" name="telp_rmh" class="form-control">
          </div>
          <div class="form-group">
            <label><b>E-mail : </b></label>
            <input type="text" name="email" class="form-control">
          </div>
        
        <h4><b>Anggota : <button class="btn btn-success" id="add-nik" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button></b></h4>

        <div class="form-group">
			<input type="text" name="nik[]" class="form-control" placeholder="NIK" style="margin-top:10px;">
			<input type="text" name="nm_anggota[]" class="form-control" placeholder="Nama">
			<div id="clone">
			
			</div>
        </div>
		
        <h4><b>Penyewaan</b></h4>
        <div class="form-group">
          <label>Sleeping Bag</label>
          <div class="input-group">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="jml_sb">
                    <span class="fa fa-minus"></span>
                  </button>
              </span>
              <input type="text" name="jml_sb" class="form-control input-sb" value="0" min="0" max="100">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="jml_sb">
                      <span class="fa fa-plus"></span>
                  </button>
              </span>
          </div>
        </div>
        <div class="form-group">
          <label>Tenda</label>
          <div class="input-group">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="jml_tenda">
                    <span class="fa fa-minus"></span>
                  </button>
              </span>
              <input type="text" name="jml_tenda" class="form-control input-tenda" value="0" min="0" max="100">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="jml_tenda">
                      <span class="fa fa-plus"></span>
                  </button>
              </span>
          </div>
        </div>
        <div class="form-group">
          <label>Peralatan Masak</label>
          <div class="input-group">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="jml_alat_masak">
                    <span class="fa fa-minus"></span>
                  </button>
              </span>
              <input type="text" name="jml_alat_masak" class="form-control input-alatmasak" value="0" min="0" max="100">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="jml_alat_masak">
                      <span class="fa fa-plus"></span>
                  </button>
              </span>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <input type="submit" name="btnSubmit" class="btn btn-primary pull-right" value="Daftar">
      </div>
      </form>
    </div>
  </div>


  <div class="spacer"></div>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

<script>
	$("#add-nik").click(function(){
        $("#clone").append('<input type="text" name="nik[]" class="form-control" placeholder="NIK" style="margin-top:10px;"><input type="text" name="nm_anggota[]" class="form-control" placeholder="Nama">');
    });
   </script>
<script type="text/javascript">
  //plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-sb').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-sb').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-sb").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });





$('.input-tenda').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-tenda').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-tenda").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });





$('.input-alatmasak').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-alatmasak').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-alatmasak").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>

</html>