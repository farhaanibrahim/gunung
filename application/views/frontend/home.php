<!DOCTYPE html>
<html>

<head>
  <title>Pendaftaran Pendakian Online</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/theme.css" type="text/css"> </head>

<body>
  <nav class="navbar navbar-expand-md bg-secondary navbar-dark">
    <div class="container">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Brand</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('news'); ?>">News</a>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('penyewaan'); ?>">Penyewaan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="<?php echo base_url('contact_us'); ?>">Contact us</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="py-5 text-center" style="background-image: url(<?php echo base_url('assets/frontend'); ?>/img/banner.png);">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12">
          <h1 class="display-3 mb-4 text-primary">Selamat Datang</h1><br><br><br><br>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 text-center bg-light">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Daftar Gunung</h1>
          <p class="lead">We grow together with the community</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 p-4">
          <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/frontend'); ?>/img/selamat.png">
          <p class="my-4"><i>Gunung Selamat dengan ketinggian 3.428 Mdpl merupakan gunung tertinggi di Jawa Tengah dan menempati urutan ke-2 gunung tertinggi di Jawa setelah Gunung Semeru</i></p><br><br><br><br><br>
          <p><b>Gunung Selamat</b><br>
            <a href="<?php echo base_url('registrasi'); ?>" class="btn btn-primary">Daftar</a>
        </div>
        <div class="col-md-4 p-4">
          <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/frontend'); ?>/img/kerinci.png">
          <p class="my-4"><i>Gunung Kerinci merupakan gunung berapi dengan ketinggian 3.805 mdpl yang sekaligus merupakan gunung berapi tertinggi se Asia Tenggara. Gunung api yang masih aktif ini terletak di area perbatasan provinsi Sumatera Barat dan Provinsi Jambi. Gunung ini memiliki berbagai julukan diantaranya yaitu Gunung Gadang, Korinci, Berapi Kurinci, dan Puncak Indrapura.</i></p>
          <p><b>Gunung Kerinci</b><br>
            <a href="<?php echo base_url('registrasi'); ?>" class="btn btn-primary">Daftar</a>
        </div>
        <div class="col-md-4 p-4">
          <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/frontend'); ?>/img/mahameru.png">
          <p class="my-4"><i>Gunung Semeru, adalah sebuah gunung berapi tertinggi di Jawa Timur dan kedua tertinggi se Jawa, tepatnya terletak di Kabupaten Lumajang. Gunung Semeru memiliki ketinggian 3,676 meter di atas permukaan laut dan juga dikenal sebagai Mahameru atau Gunung Agung.</i></p><br><br>
          <p><b>Gunung Semeru</b><br>
            <a href="<?php echo base_url('registrasi'); ?>" class="btn btn-primary">Daftar</a>
        </div>
        <div class="col-md-4 p-4">
          <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/frontend'); ?>/img/leuser.png">
          <p class="my-4"><i>Gunung Leuser adalah sebuah gunung yang terdapat di provinsi Aceh. Gunung ini memiliki elevasi 3,466 mdpl. Jika dilihat dari ketinggiannya maka gunung ini masuk dalam kategori sangat tinggi.</i></p><br><br><br>
          <p><b>Gunung Leuser</b><br>
            <a href="<?php echo base_url('registrasi'); ?>" class="btn btn-primary">Daftar</a>
        </div>
        <div class="col-md-4 p-4">
          <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/frontend'); ?>/img/rinjani.png">
          <p class="my-4"><i>Gunung Rinjani adalah gunung berapi dengan ketinggian 3.726 meter di atas permukaan laut. Predikat gunung berapi tertinggi ke dua di Indonesia pun jadi bukti megahnya puncak Anjani. Selayaknya gunung-gunung tinggi lainnya di dunia, ia menjanjikan keindahan paripurna bersanding dengan beraneka bahaya.</i></p>
          <p><b>Gunung Rinjani</b><br>
            <a href="<?php echo base_url('registrasi'); ?>" class="btn btn-primary">Daftar</a>
        </div>
        <div class="col-md-4 p-4">
          <img class="img-fluid d-block rounded-circle mx-auto" src="<?php echo base_url('assets/frontend'); ?>/img/argopuro.png">
          <p class="my-4"><i>Gunung Argopuro dengan ketingiian 3.088 merupakan salah satu gunung yang terdapat di jawa timur. Gunung ini berada di kompleks Dataran Tinggi Yang (Yang Plateau), dimana Dataran Tinggi Yang merupakan kawasan suaka margasatwa yaitu Suaka Margasatwa (SM) Dataran Tinggi Yang.</i></p><br>
          <p><b>Gunung Argopuro</b><br>
            <a href="<?php echo base_url('registrasi'); ?>" class="btn btn-primary">Daftar</a>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>