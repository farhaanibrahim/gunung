<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/theme.css" type="text/css"> </head>

<body>
  <nav class="navbar navbar-expand-md bg-secondary navbar-dark">
    <div class="container">
    <a class="navbar-brand" href="<?php echo base_url(); ?>">Brand</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('news'); ?>">News</a>
          </li>
            <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('penyewaan'); ?>">Penyewaan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="<?php echo base_url('contact_us'); ?>">Contact us</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="py-5 text-center" style="background-image: url(<?php echo base_url('assets/frontend'); ?>/img/banner.png);">
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12">
          <h1 class="display-3 mb-4 text-primary">Penyewaan</h1><br><br><br>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <img class="img-fluid d-block w-100 mb-3 rounded" src="<?php echo base_url('assets/frontend'); ?>/img/peralatan.png"><br>
              <p class="">Agar mempermudahkan pada pendaki yang belom memiliki kelengkapan peralatan pendakian, maka web ini menyediakan form penyewaan barang. Yang menyediakan berbagai peralatan yang kualitasnya masih bagus ataupun masih layak pakai, agak pendaki nyaman ketika menggunakan barang sewaan tersebut.</p>
            </div>
            <div class="col-md-6">
              <img class="img-fluid d-block w-100 mb-3 rounded" src="<?php echo base_url('assets/frontend'); ?>/img/masak.png"><br>
              <p class="">Bagaimana caranya?<br> Pertama kalian pilih lah gunung yang akan kalian daki, kemudia klik "daftar" maka form penyewaan ada di bawah form data diri. Dan disitu lah kalian bisa memilihg barang apa saja yang akan kalian sewa atau kalian pinjam.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>