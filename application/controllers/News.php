<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('DataModel');
    }

	public function index()
	{
        $data['news'] = $this->DataModel->getNews();
		$this->load->view('frontend/news',$data);
	}
}
