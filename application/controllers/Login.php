<?php 
/**
* 
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('DataModel');
	}

	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			redirect(base_url('admin'));
		} else {
			$this->load->view('backend/login');
		}
		
	}

	public function process()
	{
		if ($this->session->userdata('status') == 'login') {
	      redirect(base_url('app'));
	    } else {
	      $username = $this->input->post('username');
	      $password = md5($this->input->post('password'));

	      $data = array(
	        'username'=>$username,
	        'password'=>$password
	      );
	      $query = $this->DataModel->login($data);
	      if ($query->num_rows() > 0) {
	        foreach ($query->result() as $row) {
	          if ($row->username == $username && $row->password == $password) {
	            $session = array(
	              'username'=>$row->username,
	              'status'=>'login'
	            );
	            $this->session->set_userdata($session);
	            redirect(base_url('admin'));
	          } else {
	            $this->session->set_flashdata('error','Wrong username or password!');
	            redirect(base_url('login'));
	          }
	        }
	      } else {
	        $this->session->set_flashdata('error','Wrong username or password!');
	        redirect(base_url('login'));
	      }
	    }
	}

	public function logout()
  	{
	    if ($this->session->userdata('status') == 'login') {
	     	$this->session->sess_destroy();
	    	redirect(base_url('login'));
	    } else {
	     	redirect(base_url('login'));
	    }
  	}
}