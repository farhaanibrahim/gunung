<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('DataModel');
    }

	public function index()
	{
        $data['gunung'] = $this->DataModel->getGunung();
		$this->load->view('frontend/registrasi',$data);
    }
    
    public function proses()
    {
		$nik = $this->input->post('nik');
		$nm_anggota = $this->input->post('nm_anggota');
		
		$tujuan = $this->input->post('tujuan');
		$tgl_pendakian = $this->input->post('tgl_pendakian');
		$akhir_pendakian = $this->input->post('akhir_pendakian');
		$nm_ketua = $this->input->post('nama_ketua');
		$nik_ketua = $this->input->post('nik_ketua');
		$jk_ketua = $this->input->post('jk');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$almt_ketua = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$telp_rmh = $this->input->post('telp_rmh');
		$email_ketua = $this->input->post('email');
		
		$jml_sb =  $this->input->post('jml_sb');
		$jml_tenda = $this->input->post('jml_tenda');
		$jml_alat_masak = $this->input->post('jml_alat_masak');
		
		$data_ketua = array(
			'id_ketua'=>'',
			'nama_lengkap'=>$nm_ketua,
			'tgl_lahir'=>$tgl_lahir,
			'jenis_kelamin'=>$jk_ketua,
			'alamat_rumah'=>$almt_ketua,
			'no_ktp'=>$nik_ketua,
			'no_hp'=>$no_hp,
			'telfon_rumah'=>$telp_rmh,
			'tujuan'=>$tujuan,
			'tgl_pendakian'=>$tgl_pendakian,
			'akhir_pendakian'=>$akhir_pendakian,
			'email'=>$email_ketua,
			'status'=>'unconfirmed'
		);
		$this->DataModel->registrasi_ketua($data_ketua);
		
		$get_id_ketua = $this->DataModel->getKetuaByNIK($nik_ketua);
		
		foreach($get_id_ketua->result() as $row){
			$id_ketua = $row->id_ketua;
		}
		$data_sewa = array(
			'id_sewa'=>'',
			'id_ketua'=>$id_ketua,
			'sb'=>$jml_sb,
			'tenda'=>$jml_tenda,
			'alat_masak'=>$jml_alat_masak
		);
		$this->DataModel->registrasi_sewa($data_sewa);
		
        foreach (array_combine($nik,$nm_anggota) as $nik => $nm_anggota) {
			$data_anggota = array(
				'id_anggota'=>'',
				'id_ketua'=>$id_ketua,
				'id_gunung'=>$tujuan,
				'nama_lengkap'=>$nm_anggota,
				'no_ktp'=>$nik
			);
			$this->DataModel->registrasi_anggota($data_anggota);
		}
		
		$this->session->set_flashdata('success','Registrasi Berhasil');
		
		redirect(base_url('registrasi'));
    }
}
