<?php 
/**
* 
*/
class Admin extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('DataModel');
	}

	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			$data['gunung'] = $this->DataModel->getGunung();
			$data['count'] = $this->DataModel;
			$this->load->view('backend/dashboard',$data);
		} else {
			redirect(base_url('login'));
		}
		
	}

	public function news()
	{
		if ($this->session->userdata('status')=='login') {
			$data['news'] = $this->DataModel->getNews();
			$this->load->view('backend/news',$data);
		} else {
			redirect(base_url('login'));
		}
		
	}

	public function newsInput()
	{
		if ($this->session->userdata('status')=='login') {
			$this->load->view('backend/newsInput');
		} else {
			redirect(base_url('login'));
		}
		
	}
	public function newsInputProcess()
	{
		if ($this->session->userdata('status')=='login') {
			$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 10240;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {
                        $this->session->set_flashdata('error', $this->upload->display_errors());

                        redirect(base_url('admin/newsInput'));
                }
                else
                {
                    $data = array(
						'judul'=>$this->input->post('judul'),
						'isi'=>$this->input->post('isi'),
						'tgl'=>date('Ymd'),
						'foto'=>$this->upload->data('file_name')
					);

					$this->DataModel->newsInput($data);

					$this->session->set_flashdata('success','Data berhasil disimpan!');
					redirect(base_url('admin/news'));
                }
		} else {
			redirect(base_url('login'));
		}
		
	}

	public function newsEdit($id)
	{
		if ($this->session->userdata('status')=='login') {
			$data['news'] = $this->DataModel->getNewsByID($id);
			$this->load->view('backend/newsEdit',$data);
		} else {
			redirect(base_url('login'));
		}
		
	}

	public function newsEditProcess($id)
	{
		if ($this->session->userdata('status')=='login') {
			$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 10240;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('foto'))
                {
					$data = array(
						'judul'=>$this->input->post('judul'),
						'isi'=>$this->input->post('isi'),
						'tgl'=>date('Ymd'),
						'foto'=>$this->input->post('oldPhoto')
					);

					$this->DataModel->newsUpdate($id,$data);
					$this->session->set_flashdata('success','Data berhasil diupdate!');
					redirect(base_url('admin/news'));
                }
                else
                {
                    $data = array(
						'judul'=>$this->input->post('judul'),
						'isi'=>$this->input->post('isi'),
						'tgl'=>date('Ymd'),
						'foto'=>$this->upload->data('file_name')
					);

					$this->DataModel->newsUpdate($id,$data);

					$this->session->set_flashdata('success','Data berhasil diupdate!');
					redirect(base_url('admin/news'));
                }
		} else {
			redirect(base_url('login'));
		}
		
	}

	public function newsDelete($id)
	{
		if ($this->session->userdata('status')=='login') {
			$foto = $this->DataModel->getNewsByID($id);
			foreach ($foto->result() as $row) {
				echo $nm_foto = $row->foto;
			}

			unlink("uploads/".$nm_foto."");
			
			$this->DataModel->newsDelete($id);

			$this->session->set_flashdata('success','Data berhasil dihapus!');

			redirect(base_url('admin/news'));
		} else {
			redirect(base_url('login'));
		}
		
	}

	public function gunung()
	{
		if ($this->session->userdata('status')=='login') {
			$data['gunung'] = $this->DataModel->getGunung();
			$data['count'] = $this->DataModel;
			$this->load->view('backend/gunung',$data);
		} else {
			redirect(base_url('login'));
		}
		
	}

	public function gunungInput()
	{
		if ($this->session->userdata('status')=='login') {
			$this->load->view('backend/gunungInput');
		} else {
			redirect(base_url('login'));
		}
		
	}

	public function inputGunungProcess()
	{
		if ($this->session->userdata('status')=='login') {
			$data = array(
				'id_gunung'=>'',
				'nm_gunung'=>$this->input->post('nm_gunung'),
				'quota'=>$this->input->post('quota')
			);
			$this->DataModel->insertQuotaGunung($data);
			$this->session->set_flashdata('success','Data berhasil diinput!');
			redirect(base_url('admin/gunung'));
		} else {
			redirect(base_url('login'));
		}
	}

	public function gunungEdit($id)
	{
		if ($this->session->userdata('status')=='login') {
			/*
			$data = array(
				'nm_gunung'=>$this->input->post('nm_gunung'),
				'quota'=>$this->input->post('quota')
			);

			$this->DataModel->updateQuota($id,$data);
			$this->session->set_flashdata('success','Data berhasil diupdate!');
			redirect(base_url('admin/gunung'));
			*/
		} else {
			redirect(base_url('login'));
		}
		
	}
	
	function registrasi_pendaki()
	{
		if($this->session->userdata('status')=='login'){
			$data['pendaki'] = $this->DataModel->getDataPendaftar();
			$this->load->view('backend/daftar_registrasi_pendaki',$data);
		} else {
			redirect(base_url('admin/gunung'));
		}
	}
	
	function view_registrasi_pendaki($id_ketua)
	{
		if($this->session->userdata('status')=='login'){
			$data['pendaki'] = $this->DataModel->getDataPendaftarDetail();
			$data['anggota'] = $this->DataModel->getAnggota($id_ketua);
			$this->load->view('backend/detail_registrasi_pendaki',$data);
		} else {
			redirect(base_url('login'));
		}
	}
	
	function konfirmasi_registrasi_pendaki($id_ketua)
	{
		if($this->session->userdata('status')=='login'){
			$data['pendaki'] = $this->DataModel->getDataPendaftarDetail();
			$data['anggota'] = $this->DataModel->getAnggota($id_ketua);

			$query = $this->DataModel->getKetuaByID($id_ketua);
			foreach ($query->result() as $row) {
				$email = $row->email;
			}

			$this->DataModel->konfirmasi_pendaftaran($id_ketua);
			
			$this->load->library('pdf');

			$this->pdf->load_view('backend/detail_registrasi_pendakiPDF',$data);
			$this->pdf->set_paper('A4','potrait');
			$this->pdf->render();

			$pdf = $this->pdf->output();

			//code untuk kirim email
			//Load email library
	        $config['protocol'] = 'smtp';
	        $config['smtp_host'] = 'ssl://smtp.gmail.com';
	        $config['smtp_port'] = 465; //465 //587
	        $config['smtp_user'] = 'notif.emailer@gmail.com'; //edit email server
	        $config['smtp_pass'] = '@farhan29';  //edit
	        $config['smtp_crypto'] = 'security';
	        $config['mailtype'] = 'html';
	        $config['smtp_timeout'] = '4';
	        $config['charset'] = 'iso-8859-1';
	        $config['wordwrap'] = TRUE;
	        $config['newline'] = "\r\n"; //use double quotes
	        
	        $this->load->library('email', $config);
	        $this->email->initialize($config);

	        //send mail
	        $this->email->to($email);
	        $this->email->from('notif.emailer@gmail.com', 'Notif Emailer');
	        $this->email->subject("Bukti Pendaftaran");
	        $this->email->message("Bukti Pendaftaran");
	        $this->email->attach($pdf, 'application/pdf', "Pdf File " . date("m-d H-i-s") . ".pdf", false);

	        if ($this->email->send()) {
	            $this->session->set_flashdata('success','Konfirmasi berhasil, bukti pendaftaran berhasil dikirm via email');
				redirect(base_url('admin/registrasi_pendaki'));
	        } else {
	            show_error($this->email->print_debugger());
	        }
		} else {
			redirect(base_url('login'));
		}
	}
	
	function view_registrasi_pendakiPDF($id_ketua)
	{
		if($this->session->userdata('status')=='login'){
			$data['pendaki'] = $this->DataModel->getDataPendaftarDetail();
			$data['anggota'] = $this->DataModel->getAnggota($id_ketua);
			
			$this->load->library('pdf');

			$this->pdf->load_view('backend/detail_registrasi_pendakiPDF',$data);
			$this->pdf->set_paper('A4','potrait');
			$this->pdf->render();
			$this->pdf->stream("backend/detail_registrasi_pendakiPDF",array('Attachment'=>0),$data);
		} else {
			redirect(base_url('login'));
		}
	}
}