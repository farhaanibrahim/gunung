<?php 
/**
* 
*/
class DataModel extends CI_Model
{
	
	public function login($data)
  	{
    	$this->db->where($data);
    	return $this->db->get('t_user');
  	}

  	public function getNews()
  	{
  		return $this->db->get('news');
	}
	  
	public function newsInput($data)
	{
		$this->db->insert('news',$data);
	}

	public function getNewsByID($id)
	{
		$this->db->where('id',$id);
		return $this->db->get('news');
	}

	public function newsDelete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('news');
	}

	public function newsUpdate($id,$data)
	{
		$this->db->where('id',$id);
		$this->db->set($data);
		$this->db->update('news');
	}

	public function getGunung()
	{
		return $this->db->get('gunung');
	}

	public function insertQuotaGunung($data)
	{
		$this->db->insert('gunung',$data);
	}

	public function updateQuota($id,$data)
	{
		$this->db->where($id);
		$this->db->set($data);
		$this->db->update('gunung');
	}
	
	function getKetuaByNIK($nik)
	{
		$this->db->where('no_ktp',$nik);
		return $this->db->get('ketua_pendaki');
	}

	function getKetuaByID($id)
	{
		$this->db->where('id_ketua',$id);
		return $this->db->get('ketua_pendaki');
	}
	
	function registrasi_ketua($data)
	{
		$this->db->insert('ketua_pendaki',$data);
	}
	
	function registrasi_sewa($data)
	{
		$this->db->insert('sewa_barang',$data);
	}
	
	function registrasi_anggota($data)
	{
		$this->db->insert('anggota_pendaki',$data);
	}
	
	function getDataPendaftar()
	{
		$this->db->join('gunung','ketua_pendaki.tujuan = gunung.id_gunung');
		return $this->db->get('ketua_pendaki');
	}
	
	function getDataPendaftarDetail()
	{
		$this->db->join('sewa_barang', 'ketua_pendaki.id_ketua = sewa_barang.id_ketua');
		$this->db->join('gunung','ketua_pendaki.tujuan = gunung.id_gunung');
		return $this->db->get('ketua_pendaki');
	}
	
	function getAnggota($id_ketua)
	{
		$this->db->where('id_ketua',$id_ketua);
		return $this->db->get('anggota_pendaki');
	}
	
	function konfirmasi_pendaftaran($id_ketua)
	{
		$this->db->where('id_ketua',$id_ketua);
		$this->db->set('status','confirmed');
		$this->db->update('ketua_pendaki');
	}
	
	function getJmlPendaki($id_gunung)
	{
		$this->db->where('id_gunung',$id_gunung);
		$query = $this->db->get('anggota_pendaki')->num_rows();
		if($query > 0){
			return $query + 1;
		} else {
			return $query;
		}
	}
}