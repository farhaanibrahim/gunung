-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 09:05 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_gunung`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota_pendaki`
--

CREATE TABLE `anggota_pendaki` (
  `id_anggota` int(50) NOT NULL,
  `id_ketua` int(50) NOT NULL,
  `id_gunung` int(15) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `no_ktp` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota_pendaki`
--

INSERT INTO `anggota_pendaki` (`id_anggota`, `id_ketua`, `id_gunung`, `nama_lengkap`, `no_ktp`) VALUES
(5, 1, 1, 'Farhan', 15);

-- --------------------------------------------------------

--
-- Table structure for table `gunung`
--

CREATE TABLE `gunung` (
  `id_gunung` int(2) NOT NULL,
  `nm_gunung` varchar(25) NOT NULL,
  `quota` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gunung`
--

INSERT INTO `gunung` (`id_gunung`, `nm_gunung`, `quota`) VALUES
(1, 'Gunung Selamat', 200),
(2, 'Gunung Kerinci', 200),
(3, 'Gunung Semeru', 200),
(4, 'Gunung Leuser', 200),
(5, 'Gunung Rinjani', 200),
(6, 'Gunung Argopuro', 200);

-- --------------------------------------------------------

--
-- Table structure for table `ketua_pendaki`
--

CREATE TABLE `ketua_pendaki` (
  `id_ketua` int(50) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `alamat_rumah` text NOT NULL,
  `no_ktp` int(50) NOT NULL,
  `no_hp` int(50) NOT NULL,
  `telfon_rumah` int(50) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `tgl_pendakian` date NOT NULL,
  `akhir_pendakian` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` enum('unconfirmed','confirmed') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ketua_pendaki`
--

INSERT INTO `ketua_pendaki` (`id_ketua`, `nama_lengkap`, `tgl_lahir`, `jenis_kelamin`, `alamat_rumah`, `no_ktp`, `no_hp`, `telfon_rumah`, `tujuan`, `tgl_pendakian`, `akhir_pendakian`, `email`, `status`) VALUES
(1, 'Akbar Wibawanto', '2018-11-05', 'Laki-laki', 'ADAWDD', 10, 2147483647, 251283848, '1', '2018-11-14', '2018-11-17', 'abisonna@gmail.com', 'unconfirmed');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(15) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `tgl` date NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sewa_barang`
--

CREATE TABLE `sewa_barang` (
  `id_sewa` int(15) NOT NULL,
  `id_ketua` int(15) NOT NULL,
  `sb` int(3) NOT NULL,
  `tenda` int(3) NOT NULL,
  `alat_masak` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sewa_barang`
--

INSERT INTO `sewa_barang` (`id_sewa`, `id_ketua`, `sb`, `tenda`, `alat_masak`) VALUES
(1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id` int(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota_pendaki`
--
ALTER TABLE `anggota_pendaki`
  ADD PRIMARY KEY (`id_anggota`,`id_ketua`);

--
-- Indexes for table `gunung`
--
ALTER TABLE `gunung`
  ADD PRIMARY KEY (`id_gunung`);

--
-- Indexes for table `ketua_pendaki`
--
ALTER TABLE `ketua_pendaki`
  ADD PRIMARY KEY (`id_ketua`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sewa_barang`
--
ALTER TABLE `sewa_barang`
  ADD PRIMARY KEY (`id_sewa`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota_pendaki`
--
ALTER TABLE `anggota_pendaki`
  MODIFY `id_anggota` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gunung`
--
ALTER TABLE `gunung`
  MODIFY `id_gunung` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ketua_pendaki`
--
ALTER TABLE `ketua_pendaki`
  MODIFY `id_ketua` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sewa_barang`
--
ALTER TABLE `sewa_barang`
  MODIFY `id_sewa` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
